﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Microsoft.Samples.Kinect.ColorBasics
{
    using System;
    using System.Globalization;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Text;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Forms;
    using System.Windows.Controls;
    using System.Windows.Media.Imaging;
    using System.Runtime.InteropServices;
    using Microsoft.Kinect;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using System.Diagnostics;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor sensor;

        /// <summary>
        /// Bitmap that will hold color information
        /// </summary>
        private WriteableBitmap colorBitmap;

        /// <summary>
        /// Intermediate storage for the color data received from the camera
        /// </summary>
        private byte[] colorPixels;

        /// <summary>
        /// Intermediate storage for the color data we will generate
        /// </summary>
        private byte[] colorPixels2;

        /// <summary>
        /// Intermediate storage for the depth data received from the camera
        /// </summary>
        private short[] depthPixels;

        /// <summary>
        /// Thread to run background image analysis tasks on
        /// </summary>
        private BackgroundWorker _backgroundWorker1;

        /// <summary>
        /// The stream that is to be displayed
        /// </summary>
        private bool colorSelect = true;

        /// <summary>
        /// Defines a simple square by the upper left vertex and the side length
        /// </summary>
        private struct Square
        {
            public int x, y, length;
        }

        private static int frameWidth = 640;

        private static int frameHeight = 480;

        /// <summary>
        /// User defined limits for color searches. -1 as a lower limit ignores the search.
        /// </summary>
        private int[] colorLimits = new int[6]; // encoded [red high, green high, blue high, red low, green low, blue low]

        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent(); 
        }

        /// <summary>
        /// Execute startup tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            // Look through all sensors and start the first connected one.
            // This requires that a Kinect is connected at the time of app startup.
            // To make your app robust against plug/unplug, 
            // it is recommended to use KinectSensorChooser provided in Microsoft.Kinect.Toolkit
            foreach (var potentialSensor in KinectSensor.KinectSensors)
            {
                if (potentialSensor.Status == KinectStatus.Connected)
                {
                    this.sensor = potentialSensor;
                    break;
                }
            }

            if (null != this.sensor)
            {
                // Turn on the color stream to receive color frames
                this.sensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);

                // Turn on the depth stream to receive depth frames
                this.sensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);

                // Allocate space to put the depth pixels we'll receive
                this.depthPixels = new short[this.sensor.DepthStream.FramePixelDataLength];

                // Allocate space to put the pixels we'll receive
                this.colorPixels = new byte[this.sensor.ColorStream.FramePixelDataLength];

                // Allocate space to put the color pixels we'll create
                this.colorPixels2 = new byte[this.sensor.DepthStream.FramePixelDataLength * sizeof(int)];
                
                // This is the bitmap we'll display on-screen
                this.colorBitmap = new WriteableBitmap(this.sensor.ColorStream.FrameWidth, this.sensor.ColorStream.FrameHeight, 96.0, 96.0, PixelFormats.Bgr32, null);

                // Set the image we display to point to the bitmap where we'll put the image data
                this.Image.Source = this.colorBitmap;

                // Add an event handler to be called whenever there is new color frame data
                this.sensor.ColorFrameReady += this.SensorColorFrameReady;

                // Add an event handler to be called whenever there is new depth frame data
                this.sensor.DepthFrameReady += this.SensorDepthFrameReady;

                // Start the sensor!
                try
                {
                    this.sensor.Start();
                }
                catch (IOException)
                {
                    this.sensor = null;
                }
            }

            if (null == this.sensor)
            {
                this.statusBarText.Text = Properties.Resources.NoKinectReady;
            }

            for (int i = 0; i < colorLimits.Length; i++)
                colorLimits[i] = -1;

            _backgroundWorker1 = new BackgroundWorker();
            _backgroundWorker1.WorkerReportsProgress = false;
            _backgroundWorker1.WorkerSupportsCancellation = false;
            _backgroundWorker1.DoWork += new DoWorkEventHandler(_backgroundWorker1_DoWork);
            _backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(_backgroundWorker1_RunWorkerCompleted);

            frameWidth = (int)this.colorBitmap.Width;
            frameHeight = (int)this.colorBitmap.Height;

        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (null != this.sensor)
            {
                this.sensor.Stop();
            }
        }

        /// <summary>
        /// Event handler for Kinect sensor's ColorFrameReady event
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void SensorColorFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            using (ColorImageFrame colorFrame = e.OpenColorImageFrame())
            {
                if (colorFrame != null)
                {
                    // Copy the pixel data from the image to a temporary array
                    colorFrame.CopyPixelDataTo(this.colorPixels);

                    // Enable the color stream to view
                    if (colorSelect)
                    {
                        // Write the pixel data into our bitmap
                        this.colorBitmap.WritePixels(
                            new Int32Rect(0, 0, this.colorBitmap.PixelWidth, this.colorBitmap.PixelHeight),
                            this.colorPixels,
                            this.colorBitmap.PixelWidth * sizeof(int),
                            0);
                    }

                    // Run the image analysis when the background thread is freed up again
                    if (!_backgroundWorker1.IsBusy)
                    {
                        byte[] pixeldata = new byte[colorFrame.PixelDataLength];
                        colorFrame.CopyPixelDataTo(pixeldata);

                        //launch the image analysis on the background thread
                        _backgroundWorker1.RunWorkerAsync(pixeldata);
                    }

                }
            }
        }

        /// <summary>
        /// Event handler for Kinect sensor's DepthFrameReady event
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void SensorDepthFrameReady(object sender, DepthImageFrameReadyEventArgs e)
        {
            using (DepthImageFrame depthFrame = e.OpenDepthImageFrame())
            {
                if (depthFrame != null)
                {
                    // Copy the pixel data from the image to a temporary array
                    depthFrame.CopyPixelDataTo(this.depthPixels);

                    // Enable the depth stream to view
                    if (!colorSelect)
                    {
                        // Convert the depth to RGB
                        int colorPixelIndex = 0;

                        for (int i = 0; i < this.depthPixels.Length; ++i)
                        {
                            // discard the portion of the depth that contains only the player index
                            short depth = (short)(this.depthPixels[i] >> DepthImageFrame.PlayerIndexBitmaskWidth);


                            // to convert to a byte we're looking at only the lower 8 bits
                            // by discarding the most significant rather than least significant data
                            // we're preserving detail, although the intensity will "wrap"
                            // add 1 so that too far/unknown is mapped to black
                            byte intensity = (byte)((depth + 1) & byte.MaxValue);

                            // Write out blue byte
                            this.colorPixels2[colorPixelIndex++] = intensity;

                            // Write out green byte
                            this.colorPixels2[colorPixelIndex++] = intensity;

                            // Write out red byte                        
                            this.colorPixels2[colorPixelIndex++] = intensity;

                            // We're outputting BGR, the last byte in the 32 bits is unused so skip it
                            // If we were outputting BGRA, we would write alpha here.
                            ++colorPixelIndex;
                        }

                        // Write the pixel data into our bitmap
                        this.colorBitmap.WritePixels(
                            new Int32Rect(0, 0, this.colorBitmap.PixelWidth, this.colorBitmap.PixelHeight),
                            this.colorPixels2,
                            this.colorBitmap.PixelWidth * sizeof(int),
                            0);
                    }
                }
            }
        }



        /// <summary>
        /// Sets-up and runs the background thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            // Get the BackgroundWorker that raised this event.
            BackgroundWorker worker = sender as BackgroundWorker;

            // Assign the result of the computation
            // to the Result property of the DoWorkEventArgs
            // object. This is will be available to the 
            // RunWorkerCompleted eventhandler.
            byte[] arg = (byte[])e.Argument;

            e.Result = PerformShapeDetection(ImageToBitmap(arg));
        }

        /// <summary>
        /// Runs the post-image analysis outputs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            StringBuilder msgBuilder = new StringBuilder("\n");
            circleCanvas.Children.Clear();
            CircleF[] circles = (CircleF[])e.Result;
            foreach (CircleF circle in circles)
            {
                // discard the portion of the depth that contains only the player index
                int depth = (this.depthPixels[(int)(circle.Center.Y * frameWidth + circle.Center.X)] >> DepthImageFrame.PlayerIndexBitmaskWidth);

                if (depth <= 0)
                    depth = -1;

                int b = 0;
                int g = 0;
                int r = 0;

                Square mySquare = InscribedSquare(circle);

                
                int inc = (int)(mySquare.length / (mySquare.length / 3));
                if (inc <= 0)
                    inc = 1;
                int count = 0;
                bool pass = true;
                int index = 0;


                if (colorLimits[3] != -1 && colorLimits[4] != -1 && colorLimits[5] != -1)
                {
                    // sum up a sampling of each color data from pixels taken within a square inscribed in the detected circle
                    for (int x = mySquare.x; x < mySquare.length + mySquare.x; x += inc)
                        for (int y = mySquare.y; y < mySquare.length + mySquare.y; y += inc)
                        {
                            index = (y * frameWidth + x) * 4;
                            if (index + 2 < this.colorPixels.Length && x >= 0 && y >= 0)
                            {
                                pass = true;
                                try
                                {
                                    b += this.colorPixels[index]; // blue
                                }
                                catch (IndexOutOfRangeException e1)
                                {
                                    pass = false;
                                }
                                try
                                {
                                    g += this.colorPixels[index + 1]; // green
                                }
                                catch (IndexOutOfRangeException e1)
                                {
                                    pass = false;
                                }
                                try
                                {
                                    r += this.colorPixels[index + 2]; // red
                                }
                                catch (IndexOutOfRangeException e1)
                                {
                                    pass = false;
                                }

                                if(pass)
                                    count++;
                            }
                        }

                    // average each color over the number of samples summed
                    if (count > 0)
                    {
                        b = (int)(b / count);
                        g = (int)(g / count);
                        r = (int)(r / count);
                    }
                }

                // Implement color tracking
                if (b >= colorLimits[5] && b <= colorLimits[2] && g >= colorLimits[4] && g <= colorLimits[1] &&
                    r >= colorLimits[3] && r <= colorLimits[0] || colorLimits[3] == -1 || colorLimits[4] == -1 || colorLimits[5] == -1)
                {
                    PointF center = new PointF(circle.Center.X, circle.Center.Y);
                    SizeF size = new SizeF(circle.Radius, circle.Radius);
                    System.Windows.Shapes.Ellipse myEllipse = new System.Windows.Shapes.Ellipse();
                    myEllipse.Stroke = System.Windows.Media.Brushes.Red;
                    myEllipse.StrokeThickness = 5;
                    myEllipse.Fill = null;
                    myEllipse.VerticalAlignment = VerticalAlignment.Center;
                    myEllipse.Width = circle.Radius * 2;
                    myEllipse.Height = circle.Radius * 2;
                    Canvas.SetTop(myEllipse, (double)(circle.Center.Y - circle.Radius));
                    Canvas.SetLeft(myEllipse, (double)(circle.Center.X - circle.Radius));
                    circleCanvas.Children.Add(myEllipse);

                    count = 0;
                    index = 0;
                    pass = true;
                    int myDepth = 0;

                    // sum up a sampling of depth data from pixels taken within a square inscribed in the detected circle
                    for (int x = mySquare.x; x < mySquare.length + mySquare.x; x += inc)
                        for (int y = mySquare.y; y < mySquare.length + mySquare.y; y += inc)
                        {
                            index = (y * frameWidth + x);
                            if (index < this.depthPixels.Length && x >= 0 && y >= 0)
                            {
                                pass = true;
                                try
                                {
                                    myDepth += (this.depthPixels[index] >> DepthImageFrame.PlayerIndexBitmaskWidth);
                                }
                                catch(IndexOutOfRangeException e1)
                                {
                                    pass = false;
                                }
                                if(pass)
                                    count++;
                            }
                        }

                    myDepth = myDepth / count;
                    string myDepthString;
                    if (myDepth <= 0)
                        myDepthString = "N/A";
                    else
                        myDepthString = myDepth.ToString();

                    msgBuilder.Append(circle.Center.ToString() + " Radius " + circle.Radius.ToString() + " Depth: " + myDepthString + "\n"  + "\n");
                }
            }

            circleStatus.Text = "Circles ( " + circles.Length.ToString() + " ):\n" + msgBuilder.ToString();
        }

        /// <summary>
        /// Converts a Kinect ColorImageFrame to a bitmap
        /// </summary>
        /// <param name="Image">The ColorImageFrame from the RGB camera on the Kinect</param>
        /// <returns>A bitmap of Image</returns>
        private Bitmap ImageToBitmap(byte[] pixeldata)
        {
            Bitmap bmap = new Bitmap(frameWidth, frameHeight, System.Drawing.Imaging.PixelFormat.Format32bppRgb);

            BitmapData bmapdata = bmap.LockBits(
                new Rectangle(0, 0, frameWidth, frameHeight),
                ImageLockMode.WriteOnly,
                bmap.PixelFormat);

            IntPtr ptr = bmapdata.Scan0;
            Marshal.Copy(pixeldata, 0, ptr, pixeldata.Length);

            bmap.UnlockBits(bmapdata);
            return bmap;
        }

        /// <summary>
        /// Handles the user clicking on the screenshot button
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void ButtonScreenshotClick(object sender, RoutedEventArgs e)
        {
            if (null == this.sensor)
            {
                this.statusBarText.Text = Properties.Resources.ConnectDeviceFirst;
                return;
            }

            // create a png bitmap encoder which knows how to save a .png file
            BitmapEncoder encoder = new PngBitmapEncoder();

            // create frame from the writable bitmap and add to encoder
            encoder.Frames.Add(BitmapFrame.Create(this.colorBitmap));

            string time = System.DateTime.Now.ToString("hh'-'mm'-'ss", CultureInfo.CurrentUICulture.DateTimeFormat);

            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures), "KinectSnapshot-" + time + ".png");

            // write the new file to disk
            try
            {
                using (FileStream fs = new FileStream(path, FileMode.Create))
                {
                    encoder.Save(fs);
                }

                this.statusBarText.Text = string.Format("{0} {1}", Properties.Resources.ScreenshotWriteSuccess, path);
            }
            catch (IOException)
            {
                this.statusBarText.Text = string.Format("{0} {1}", Properties.Resources.ScreenshotWriteFailed, path);
            }
        }

        /// <summary>
        /// Searches for circle in single frame
        /// </summary>
        /// <param name="fileName">name of the file frame is saved to</param>
        /// <returns>overlay bitmap</returns>
        private CircleF[] PerformShapeDetection(Bitmap fileName)
          {
              if (fileName.Size.Width != 0)
              {
                  StringBuilder msgBuilder = new StringBuilder("Performance: ");

                  //Load the image from file and resize it for display
                  Image<Bgr, Byte> img =
                     new Image<Bgr, byte>(fileName)
                     .Resize(frameWidth, frameHeight, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR, true);

                  //Convert the image to grayscale and filter out the noise
                  Image<Gray, Byte> gray = img.Convert<Gray, Byte>().PyrDown().PyrUp();

                  #region circle detection
                  Stopwatch watch = Stopwatch.StartNew();
                  Gray cannyThreshold = new Gray(180);
                  Gray circleAccumulatorThreshold = new Gray(120);
                  CircleF[] circles = gray.HoughCircles(
                      cannyThreshold,
                      circleAccumulatorThreshold,
                      2.0, //Resolution of the accumulator used to detect centers of the circles
                      20.0, //min distance 
                      5, //min radius
                      0 //max radius
                      )[0]; //Get the circles from the first channel
                  watch.Stop();
                  msgBuilder.Append(String.Format("Hough circles - {0} ms; ", watch.ElapsedMilliseconds));
                  #endregion

                  return circles;
              }
              else
              {
                  return null;
              }
          }

        /// <summary>
        /// Calculate the upper-left vertex and side lengths of a square inscribed in a circle
        /// </summary>
        /// <param name="myCircle">the cirlce to inscribe a square in</param>
        /// <returns>an inscribed square</returns>
        private Square InscribedSquare(CircleF myCircle)
        {
            Square mySquare;
            
            mySquare.length = (int)( (Math.Cos(Math.PI / 4) * myCircle.Radius * 2) / 2);
            mySquare.x = (int)(myCircle.Center.X - mySquare.length / 2);
            mySquare.y = (int)(myCircle.Center.Y - mySquare.length / 2);
            return mySquare;
        }

        /// <summary>
        /// Switches the Kinect image source between  depth and color
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void streamChoice_Click(object sender, RoutedEventArgs e)
        {
            if (colorSelect)
            {
                colorSelect = false;
                streamChoice.Content = "Color";
            }
            else
            {
                colorSelect = true;
                streamChoice.Content = "Depth";
            }
        }


        /// <summary>
        /// Converts a string to an int
        /// </summary>
        /// <param name="arg">String to be converted</param>
        /// <returns></returns>
        private int stringToNumber(String arg)
        {
            int result = -1;

            try
            {
                result = Convert.ToInt32(arg);
            }
            catch (FormatException e)
            {
            }
            catch (OverflowException e)
            {
            }

            return result;
        }

        /// <summary>
        /// Detects when the limit on the user interface changes to change the color limit in the program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void redUp_TextChanged(object sender, TextChangedEventArgs e)
        {
            colorLimits[0] = stringToNumber(redUp.Text);
        }

        private void greenUp_TextChanged(object sender, TextChangedEventArgs e)
        {
            colorLimits[1] = stringToNumber(greenUp.Text);
        }

        private void blueUp_TextChanged(object sender, TextChangedEventArgs e)
        {
            colorLimits[2] = stringToNumber(blueUp.Text);
        }

        private void redDown_TextChanged(object sender, TextChangedEventArgs e)
        {
            colorLimits[3] = stringToNumber(redDown.Text);
        }

        private void greenDown_TextChanged(object sender, TextChangedEventArgs e)
        {
            colorLimits[4] = stringToNumber(greenDown.Text);
        }

        private void blueDown_TextChanged(object sender, TextChangedEventArgs e)
        {
            colorLimits[5] = stringToNumber(blueDown.Text);
        }

    }
}