To open the code, use any general purpose editor or IDE. Visual Studio works the best if you have it, otherwise Geany is good lightweight alternative and can be found here http://www.geany.org/


You will need .NET framework 4.0 and the Kinect runtime to run this demo


Go into /Bin/Release and run the ColorBasic-WPF.exe to launch the software. You will need your Kinect for Windows plugged into the USB port on your computer before launching the software. Windows should automatically detect and download the drivers needed to run the Kinect once you have the Kinect runtime installed.


To develop with the Kinect using this example you will need
A Kinect (not from an Xbox)
OpenCV
	http://opencv.willowgarage.com/wiki/Welcome
Kinect for Windows
	http://www.microsoft.com/en-us/kinectforwindows/
CMake
	http://www.cmake.org/
Microsoft Visual Studio or equivalent C# compiler
	http://www.microsoft.com/visualstudio/en-us/products/2010-editions/express
	http://www.mono-project.com/Main_Page (alternative - Mono C# compiler)
Emgu OpenCV C# wrapper
	http://www.emgu.com

For more information on getting started with this example and the Kinect in general, check out http://www.mngofirst.org/development-resources.html for detailed resources.


You can reach the developer at gofirst.robots@gmail.com with any questions, feature requests, or bug fixes.
